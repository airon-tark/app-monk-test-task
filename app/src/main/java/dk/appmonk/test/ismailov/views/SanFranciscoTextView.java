package dk.appmonk.test.ismailov.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;
import dk.appmonk.test.ismailov.util.TypefaceUtil;

public class SanFranciscoTextView extends TextView {

    /*************************************
     * PUBLIC METHODS
     *************************************/
    public SanFranciscoTextView(Context context) {
        super(context);
        TypefaceUtil.initFontSanFrancisco(this);
    }

    public SanFranciscoTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypefaceUtil.initFontSanFrancisco(this);
    }

    public SanFranciscoTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        TypefaceUtil.initFontSanFrancisco(this);
    }

}
