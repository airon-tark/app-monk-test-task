package dk.appmonk.test.ismailov.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;
import dk.appmonk.test.ismailov.util.TypefaceUtil;

public class SanFranciscoEditText extends EditText {

    /*************************************
     * PUBLIC METHODS
     *************************************/
    public SanFranciscoEditText(Context context) {
        super(context);
        TypefaceUtil.initFontSanFrancisco(this);
    }

    public SanFranciscoEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypefaceUtil.initFontSanFrancisco(this);
    }

    public SanFranciscoEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        TypefaceUtil.initFontSanFrancisco(this);
    }

}
