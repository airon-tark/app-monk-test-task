package dk.appmonk.test.ismailov.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ImageButton;

/**
 * User: Pavel
 * Date: 19.03.2015
 * Time: 17:04
 */
public class CustomImageButton extends ImageButton {

    /**************************************
     * PRIVATE STATIC CONSTANTS
     **************************************/
    private static final String TAG = CustomImageButton.class.getSimpleName();

    /**************************************
     * PUBLIC STATIC CONSTANTS
     **************************************/
    public static final float ALPHA_PRESSED = 0.4f;
    public static final float ALPHA_IDLE = 1.0f;

    /**************************************
     * PRIVATE FIELDS
     **************************************/
    private boolean mTouchEventsEnabled = true;

    /**************************************
     * CONSTRUCTORS
     **************************************/
    public CustomImageButton(Context context) {
        super(context);
        setMyPressed(false);
    }

    public CustomImageButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        setMyPressed(false);
    }

    public CustomImageButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setMyPressed(false);
    }

    /**************************************
     * PUBLIC METHODS
     **************************************/
    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if (!mTouchEventsEnabled) {
            return super.onTouchEvent(event);
        }

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                setMyPressed(true);
                break;
            case MotionEvent.ACTION_UP:
                setMyPressed(false);
                break;
            case MotionEvent.ACTION_CANCEL:
                setMyPressed(false);
                break;
            case MotionEvent.ACTION_HOVER_ENTER:
                setMyPressed(true);
                break;
            case MotionEvent.ACTION_HOVER_EXIT:
                setMyPressed(false);
                break;
            case MotionEvent.ACTION_OUTSIDE:
                setMyPressed(false);
                break;
        }

        return super.onTouchEvent(event);
    }

    public void setTouchEventsEnabled(boolean isEnabled) {
        mTouchEventsEnabled = isEnabled;
    }

    /**************************************
     * PRIVATE METHODS
     **************************************/
    private void setMyPressed(boolean pressed) {
        //Debug.log(TAG, "setMyPressed");
        if (pressed) {
            android.support.v4.view.ViewCompat.setAlpha(this, ALPHA_PRESSED);
        } else {
            android.support.v4.view.ViewCompat.setAlpha(this, ALPHA_IDLE);
        }
    }

}
