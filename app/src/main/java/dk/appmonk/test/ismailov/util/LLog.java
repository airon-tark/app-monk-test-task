package dk.appmonk.test.ismailov.util;

import android.util.Log;
import java.util.Arrays;

public class LLog {

    /*************************************
     * PUBLIC STATIC VARIABLES
     *************************************/
    public static final int MIN_TAG_LENGTH = 30;

    /*************************************
     * PRIVATE STATIC VARIABLES
     *************************************/
    private static boolean sDebuggable = false;

    /*************************************
     * PUBLIC STATIC METHODS
     *************************************/
    public static void setDebuggable(boolean isDebuggable) {
        sDebuggable = isDebuggable;
    }

    public static void e(Object o, String message) {
        if (sDebuggable) {
            Log.e(createTag(o), message);
        }
    }

    /*************************************
     * PRIVATE STATIC METHODS
     *************************************/
    private static String getMethodName() {
        /**
         * Pavel:
         * I am not sure why, but sometimes I see in logs messages like this = "onStart_aroundBody28"
         * Is contains method name + "_aroundBody%%", where %% is some random number
         * I suggest this is problem of hugo plugin
         * Anyway I get rid of that by split and getting first part before "_"
         */
        return Thread.currentThread().getStackTrace()[4].getMethodName().split("_")[0];
    }

    private static String createTag(Object object) {
        if (object instanceof String) {
            return padStart((String) object, MIN_TAG_LENGTH);
        } else {
            return createTag(object.getClass());
        }
    }

    private static String createTag(Class clazz) {
        //return clazz.getSimpleName().isEmpty() ? clazz.getName() : clazz.getSimpleName();
        return clazz.getSimpleName().isEmpty() ? clazz.getName() : padStart(clazz.getSimpleName(), MIN_TAG_LENGTH);
    }

    private static String createMessage(String methodName, Object... args) {
        //return "? [" + new Date() + "] " + methodName + '(' + (args.length > 0 ? Arrays.toString(args) : "") + ')';
        return methodName + (args.length > 0 ? " (" + Arrays.toString(args) + ")" : "");
    }

    public static String padStart(String s, int length) {
        /**
         * Pavel: changed  to "-->" and move it to tag to better looking in IDEA output
         */
        StringBuilder sb = new StringBuilder();
        sb.append("-->");
        for (int i = 0; i < length - s.length(); i++) {
            sb.append("\u0020");
        }
        sb.append(s);
        return new String(sb);
    }

}
