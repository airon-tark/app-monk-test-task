package dk.appmonk.test.ismailov.util;

import android.graphics.Typeface;
import android.widget.TextView;

/**
 * User: Pavel
 * Date: 18.03.2015
 * Time: 16:30
 */
public class TypefaceUtil {

    /*************************************
     * PUBLIC STATIC METHODS
     *************************************/
    public static void initFontSanFrancisco(TextView v) {
        initFont(v, "SanFrancisco-Regular.ttf");
    }

    /*************************************
     * PRIVATE STATIC METHODS
     *************************************/
    private static void initFont(TextView v, String fontName) {
        Typeface font = Typeface.createFromAsset(v.getContext().getAssets(), fontName);
        v.setTypeface(font);
    }

}
