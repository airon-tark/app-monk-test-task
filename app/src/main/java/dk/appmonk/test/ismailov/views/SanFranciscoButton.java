package dk.appmonk.test.ismailov.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;
import dk.appmonk.test.ismailov.util.TypefaceUtil;

public class SanFranciscoButton extends Button {

    /*************************************
     * PUBLIC METHODS
     *************************************/
    public SanFranciscoButton(Context context) {
        super(context);
        TypefaceUtil.initFontSanFrancisco(this);
    }

    public SanFranciscoButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypefaceUtil.initFontSanFrancisco(this);
    }

    public SanFranciscoButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        TypefaceUtil.initFontSanFrancisco(this);
    }

}
