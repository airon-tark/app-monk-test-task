package dk.appmonk.test.ismailov;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.LinearLayout;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dk.appmonk.test.ismailov.util.IntentUtils;
import dk.appmonk.test.ismailov.util.LLog;

public class MainActivity extends ActionBarActivity {

    /*************************************
     * BINDS
     *************************************/
    @Bind(R.id.bottom_buttons_layout)
    LinearLayout mBottomButtonsLayout;
    @Bind(R.id.central_panel_layout)
    LinearLayout mCentralPanelLayout;

    /*************************************
     * PUBLIC METHODS
     *************************************/
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        LLog.setDebuggable(BuildConfig.DEBUG);

        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                            View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
                            View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
            );
            getWindow().setStatusBarColor(Color.TRANSPARENT);
            getWindow().setNavigationBarColor(Color.TRANSPARENT);

            // little up buttons over the soft buttons
            mBottomButtonsLayout.setPadding(0, 0, 0, getResources().getDimensionPixelOffset(
                    R.dimen.bottom_buttons_layout_padding_lollipop));

            // little up central panel to looks netter with the upper buttons
            mCentralPanelLayout.setPadding(0, getResources().getDimensionPixelOffset(
                    R.dimen.central_panel_layout_padding_lollipop), 0, 0);

        }
    }

    @OnClick(R.id.go_back_link)
    public void onGoBackClick() {
        finish();
    }

    @OnClick(R.id.forgot_password_link)
    public void onForgotPasswordClick() {
        IntentUtils.openUrl(this, R.string.forgot_your_password_url);
    }

}
